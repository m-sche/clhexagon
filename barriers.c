/*
 * barriers.c
 *
 * Generate barrier patterns in the FUTURE layer of the hexagon.
 *
 * Copyright (C) 2013 Martin Scheubrein
 *
 * This program is a part of the CLHexagon game, and may be freely redistributed
 * under the terms of the GNU General Public License version 3.
 */

#include <stdlib.h>

#include "defs.h"

#define LOOP 88

void fill_hexagon(const int layer, const int barrier)
{
	int segment;

	for (segment = 0; segment < 6; segment++)
		hexagon[layer][segment] = barrier;
}

static void spiral(int cycle)
{
	fill_hexagon(FUTURE, 0);
	hexagon[FUTURE][cycle%6] = 1;
}

static void antispiral(int cycle)
{
	fill_hexagon(FUTURE, 0);
	hexagon[FUTURE][5-cycle%6] = 1;
}

static void hexahole(int cycle)
{
	if (cycle%4 == 0) {
		fill_hexagon(FUTURE, 1);
		hexagon[FUTURE][rand()%6] = 0;
	} else {
		fill_hexagon(FUTURE, 0);
	}
}

static void radioactive(int cycle)
{
	fill_hexagon(FUTURE, 0);
	if (cycle%2 == 0) {
		hexagon[FUTURE][cycle%4 < 2] = 1;
		hexagon[FUTURE][2] = hexagon[FUTURE][0];
		hexagon[FUTURE][3] = hexagon[FUTURE][1];
		hexagon[FUTURE][4] = hexagon[FUTURE][0];
		hexagon[FUTURE][5] = hexagon[FUTURE][1];
	}
}

/* Generate the FUTURE layer of barriers, needs improvement */
void generate_barriers(int cycle)
{
	/* Relax */
	switch (cycle%LOOP) {
	case 12:
	case 38:
	case 50:
	case 74:
		fill_hexagon(FUTURE, 0);
		return;
	}

	/* Work */
	if (cycle%LOOP < 11)
		spiral(cycle);
	else if (cycle%LOOP < 38)
		hexahole(cycle);
	else if (cycle%LOOP < 50)
		antispiral(cycle);
	else if (cycle%LOOP < 74)
		hexahole(cycle);
	else
		radioactive(cycle);
}
