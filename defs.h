/*
 * defs.h
 * Global definitions of clhexagon
 *
 * Martin Scheubrein, 2013
 */

#define MAJOR_VERSION 1
#define MINOR_VERSION 5

#define FUTURE	8
#define KEY_ESC 27

#define eputs(msg)	fputs(msg, stderr)

extern int hexagon[9][6];	/* Hexagon field */
