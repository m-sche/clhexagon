# clhexagon #

Command-line clone of the Super Hexagon game

## Usage ##

clhexagon [OPTIONS]  
  
OPTIONS:  

+ -d DELAY
	+ Set DELAY between barriers' moves in ms (default 200)  
+ -g
	+ Play the game in debugging mode  
+ -h
	+ Print this help box and quit  
+ -s
	+ Show the best score table and quit  
+ -v
	+ Print version and quit  
+ -x
	+ Use experimental keyboard settings; needs X server  

## Controls ##

Move		arrow keys / A,D  
Quit		Q / Esc  
Restart		any other key  

## Requirements ##

CLHexagon requires ncurses library to be installed. On Debian-based operating systems, the packages `libncurses5` and `libncurses5-dev` contain it.

## Copying ##

Copyright (C) 2013, 2014 Martin Scheubrein  
Licensed under the terms of GNU GPLv3 (see COPYING)  
