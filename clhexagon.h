/*
 * clhexagon.h
 * Function prototypes and global variables
 *
 * Martin Scheubrein, 2013
 */

#define TIMEOUT	50	/* Timeout of getch() in ms */

/* From main.c */
extern int arpt;	/* Auto rpt; when -1, x mode disabled */

void print_dbg_info(int player_pos, int delay, int time);
int write_score(const int score);
void quitxmode(void);

/* From engine.c */
int play_game(int debug, int delay);

/* From draw.c */
extern int st_row, st_col;	/* Starting point for drawing [0;0] */

void draw_background(void);
void draw_player(const char ch, const int player_pos);
void draw_barrier(const int layer, const int segment, const int barrier);
void draw_msg(const char msg[5]);
void draw_time(const int sec);

/* From barriers.h */
void generate_barriers(int cycle);
void fill_hexagon(const int layer, const int barrier);
