/*
 * draw.c
 *
 * Utilities drawing actual state of the game to the screen.
 *
 * Copyright (C) 2013 Martin Scheubrein
 *
 * This program is a part of the CLHexagon game, and may be freely redistributed
 * under the terms of the GNU General Public License version 3.
 */

#include <ncurses.h>

#include "defs.h"

int st_row, st_col;

/* Draw message at the top of the middle hexagon */
void draw_msg(const char msg[5])
{
	mvprintw(st_row+11, st_col+21, msg);
}

/* Draw time in the middle hexagon */
void draw_time(const int sec)
{
	mvprintw(st_row+12, st_col+21, "%d:%02d", sec/60, sec%60);
}

/* Draw the neighborhood of the inner hexagon */
void draw_player(const char ch, const int player_pos)
{
	mvaddch(st_row+9, st_col+21, player_pos == 0 ? ch : '_');
	mvaddch(st_row+9, st_col+24, player_pos == 1 ? ch : '_');
	mvaddch(st_row+10, st_col+27, player_pos == 2 ? ch : ' ');
	mvaddch(st_row+11, st_col+28, player_pos == 3 ? ch : '_');
	mvaddch(st_row+12, st_col+28, player_pos == 4 ? ch : ' ');
	mvaddch(st_row+13, st_col+27, player_pos == 5 ? ch : ' ');
	mvaddch(st_row+14, st_col+24, player_pos == 6 ? ch :
						(hexagon[0][3] ? '_' : ' '));
	mvaddch(st_row+14, st_col+21, player_pos == 7 ? ch :
						(hexagon[0][3] ? '_' : ' '));
	mvaddch(st_row+13, st_col+18, player_pos == 8 ? ch : ' ');
	mvaddch(st_row+12, st_col+17, player_pos == 9 ? ch : ' ');
	mvaddch(st_row+11, st_col+17, player_pos == 10 ? ch : '_');
	mvaddch(st_row+10, st_col+18, player_pos == 11 ? ch : ' ');
}

/* Draw a given barrier */
void draw_barrier(const int layer, const int segment, const int barrier)
{
	int pos;	/* Position of char in hexagon */
	char ch;	/* Written character */
	const char barriers[6] = {'_', '\\', '/', '_', '\\', '/'};

	ch = barrier ? barriers[segment] : ' ';
	switch (segment) {
	case 0:
		move(st_row+8-layer, st_col+19-layer);
		for (pos = 0; pos < 2*(layer+4); pos++)
			addch(ch);
		break;
	case 1:
		for (pos = 0; pos < layer+3; pos++) {
			move(st_row+9+pos-layer, st_col+pos+27+layer);
			addch(!barrier && pos == layer+2 ? '_' : ch);
		}
		break;
	case 2:
		for (pos = 0; pos < layer+3; pos++) {
			move(st_row+14-pos+layer, st_col+pos+27+layer);
			addch(ch);
		}
		break;
	case 3:
		move(st_row+14+layer, st_col+20-layer);
		for (pos = 0; pos < 2*(layer+3); pos++)
			addch(ch);
		break;
	case 4:
		for (pos = 0; pos < layer+3; pos++) {
			move(st_row+14-pos+layer, st_col-pos+18-layer);
			addch(ch);
		}
		break;
	case 5:
		for (pos = 0; pos < layer+3; pos++) {
			move(st_row+9+pos-layer, st_col-pos+18-layer);
			addch(!barrier && pos == layer+2 ? '_' : ch);
		}
		break;
	}
}

/* Draw background */
void draw_background(void)
{
	mvaddstr(st_row, st_col,	"           ________________________             ");
	mvaddstr(st_row+1, st_col,	"          /\\                      /\\          ");
	mvaddstr(st_row+2, st_col,	"         /  \\                    /  \\         ");
	mvaddstr(st_row+3, st_col,	"        /    \\                  /    \\        ");
	mvaddstr(st_row+4, st_col,	"       /      \\                /      \\       ");
	mvaddstr(st_row+5, st_col,	"      /        \\              /        \\      ");
	mvaddstr(st_row+6, st_col,	"     /          \\            /          \\     ");
	mvaddstr(st_row+7, st_col,	"    /            \\          /            \\    ");
	mvaddstr(st_row+8, st_col,	"   /              \\        /              \\   ");
	mvaddstr(st_row+9, st_col,	"  /                \\______/                \\  ");
	mvaddstr(st_row+10, st_col,	" /                 /      \\                 \\ ");
	mvaddstr(st_row+11, st_col,	"/_________________/        \\_________________\\");
	mvaddstr(st_row+12, st_col,	"\\                 \\        /                 /");
	mvaddstr(st_row+13, st_col,	" \\                 \\______/                 / ");
	mvaddstr(st_row+14, st_col,	"  \\                /      \\                /  ");
	mvaddstr(st_row+15, st_col,	"   \\              /        \\              /   ");
	mvaddstr(st_row+16, st_col,	"    \\            /          \\            /    ");
	mvaddstr(st_row+17, st_col,	"     \\          /            \\          /     ");
	mvaddstr(st_row+18, st_col,	"      \\        /              \\        /      ");
	mvaddstr(st_row+19, st_col,	"       \\      /                \\      /       ");
	mvaddstr(st_row+20, st_col,	"        \\    /                  \\    /        ");
	mvaddstr(st_row+21, st_col,	"         \\  /                    \\  /         ");
	mvaddstr(st_row+22, st_col,	"          \\/______________________\\/          ");
	mvaddstr(st_row+23, st_col,	"                                                ");
}
