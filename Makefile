# Makefile
#
# Compile source files into an EXE executable. CC has to be a gcc-compatible
# compiler & linker (gcc and clang tested)
#
# Martin Scheubrein, 2013

EXE	= clhexagon
CC	= gcc
CFLAGS	= -Wall -O1
LIBS	= -lncurses
OBJ	= main.o engine.o barriers.o draw.o

$(EXE): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $@ $(LIBS)

%.o: %.c
	$(CC) -c $(CFLAGS) $<

main.o: clhexagon.h defs.h
engine.o: clhexagon.h defs.h
barriers.o: defs.h
draw.o: defs.h

.PHONY: clean
clean:
	rm -f $(OBJ) $(EXE)
