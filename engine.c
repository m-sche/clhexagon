/*
 * engine.c
 *
 * Core procedures of the game. Entry point play_game() processes one round
 * of the game and then returns back to main().
 *
 * Copyright (C) 2013, 2014 Martin Scheubrein
 *
 * This program is a part of the CLHexagon game, and may be freely redistributed
 * under the terms of the GNU General Public License version 3.
 */

#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <math.h>
#include <ctype.h>

#include "clhexagon.h"
#include "defs.h"

static volatile int next_cycle_flag;	/* Being set to 1 by timer */
static int sigalrm_lock;		/* Lock for SIGALRM interrupts */

static void next_cycle(int signum);	/* Called by timer (SIGALRM) */

/* Move barriers on the screen */
static void move_barriers(void)
{
	int segm, layer;

	for (layer = 0; layer < FUTURE; layer++) {
		for (segm = 0; segm < 6; segm++) {
			if (hexagon[layer+1][segm] && !hexagon[layer][segm])
				draw_barrier(layer, segm, 1);
			if (!hexagon[layer+1][segm] && hexagon[layer][segm])
				draw_barrier(layer, segm, 0);
		}
	}
}

/* Move bariers in the array */
static void update_hexagon(void)
{
	int layer, segment;

	for (layer = 0; layer < FUTURE; layer++)
		for (segment = 0; segment < 6; segment++)
			hexagon[layer][segment] = hexagon[layer+1][segment];
}

/* When performing an atomic operation, disable SIGALRM interrupts */
static inline void mutex_lock(int *lock)
{
	*lock = 1;
}

/* Enable SIGALRM and if the signal has aready arrived, react */
static void mutex_unlock(int *lock)
{

	*lock = 0;
	if (next_cycle_flag)
		next_cycle(0);
}

static inline void mutex_relock(int *lock)
{
	mutex_unlock(lock);
	mutex_lock(lock);
}

static void next_cycle(int signum)
{
	static int cycle;		/* Cycles since the game has started */
	static int level;

	level++;

	if (sigalrm_lock) {
		next_cycle_flag = 1;
	} else {
		mutex_lock(&sigalrm_lock);
		cycle++;
		move_barriers();	/* Update the screen */
		update_hexagon();	/* Then update the array */
		generate_barriers(cycle);
		next_cycle_flag = 0;	/* Ignore too fast interrupts */
		mutex_unlock(&sigalrm_lock);
	}
}

static inline void clear_hexagon(void)
{
	int layer;

	for (layer = 0; layer <= FUTURE; layer++)
		fill_hexagon(layer, 0);
}

/* If the player dies, return 1 */
static inline int dead(const int player_pos, const int debug)
{
	if (debug)
		return 0;
	if (hexagon[0][player_pos>>1])
		return 1;

	return 0;
}

/* End the game and ask for repetition */
static int game_over(const int player_pos)
{
	draw_msg("LOSE");
	draw_player('#', player_pos);

	int key = 0;

	timeout(-1);	/* Wait for user to press a key */
	while (1) {
		key = toupper(getch());
		switch (key) {
		case 'Q':
		case KEY_ESC:
			return 0;
		case 'A':
		case 'D':
		case KEY_LEFT:
		case KEY_RIGHT:
			break;
		default:
			timeout(TIMEOUT);
			return 1;
		}
	}
}

static int player_move(int *player_pos)
{
	switch (toupper(getch())) {
	case 'Q':
	case KEY_ESC:
		if (arpt != -1)
			quitxmode();
		endwin();
		exit(0);
	case 'A':
	case KEY_LEFT:
		if (*player_pos == 0)
			*player_pos = 11;
		else
			(*player_pos)--;
		break;
	case 'D':
	case KEY_RIGHT:
		if (*player_pos == 11)
			*player_pos = 0;
		else
			(*player_pos)++;
		break;
	default:
		return 0;
	}
	return 1;
}

int play_game(const int debug, const int delay)
{
	int player_pos;			/* Player position */
	time_t st_time;			/* Absolute starting time */
	time_t time_sec;		/* Current time */
	struct itimerval wakeup;	/* Main timer */

	/* Initial drawing */
	draw_background();
	draw_msg("TIME");

	/* Initial values */
	time_sec = time(NULL);
	srand((int)time_sec);
	st_time = time_sec;
	player_pos = rand()%12;
	clear_hexagon();

	/* Set the timer */
	wakeup.it_interval.tv_sec = delay/1000;
	wakeup.it_interval.tv_usec = (delay%1000)*1000;
	wakeup.it_value.tv_sec = delay/1000;
	wakeup.it_value.tv_usec = (delay%1000)*1000;
	setitimer(ITIMER_REAL, &wakeup, NULL);

	signal(SIGALRM, next_cycle);

	draw_player('O', player_pos);

	/* Start the game */
	while (!dead(player_pos, debug)) {
		time_sec = time(NULL);

		mutex_lock(&sigalrm_lock);
		if (player_move(&player_pos) || debug) {
			mutex_relock(&sigalrm_lock);
			draw_player('O', player_pos);
		}

		mutex_relock(&sigalrm_lock);
		draw_time(time_sec-st_time);
		mutex_unlock(&sigalrm_lock);

		if (debug) {
			mutex_lock(&sigalrm_lock);
			print_dbg_info(player_pos, delay, (int)(time_sec-st_time));
			mutex_unlock(&sigalrm_lock);
		}
	}

	/* Stop timer */
	timerclear(&wakeup.it_interval);
	timerclear(&wakeup.it_value);
	setitimer(ITIMER_REAL, &wakeup, NULL);

	write_score(time_sec-st_time);
	return game_over(player_pos);
}
