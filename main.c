/*
 * main.c
 *
 * Init functions, option parsing, etc.
 *
 * Copyright (C) 2013 Martin Scheubrein
 *
 * This program is a part of the CLHexagon game, and may be freely redistributed
 * under the terms of the GNU General Public License version 3.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include <string.h>

#include "defs.h"
#include "clhexagon.h"

#define DELAY		200		/* Default delay */
#define DBGBAR_WIDTH	11		/* Width of the Debug Info bar */
#define SCOREFILE_PATH	"/.clh.scores"		/* Scorefile in home dir */
#define TEMP_XSETQ	"/tmp/clhexagon.tmp"	/* Temporary 'xset q' file */

int hexagon[9][6];	/* Hexagon field */
int arpt = -1;		/* Auto repeat; [-1, 0, 1] == [disable, off, on] */

static char *scorefile;		/* Score file location */
static int ws_row, ws_col;	/* Screen size */

static void print_help(void)
{
	printf(
	"\nUsage:\nclhexagon [OPTIONS]\n\nOPTIONS:\n"
	"  -d DELAY\tSet DELAY between barriers' moves (default %d)\n"
	"  -g\t\tShow some debug information and be immortal\n"
	"  -h\t\tPrint this help box and quit\n"
	"  -s\t\tShow the best score table and quit\n"
	"  -v\t\tPrint version and quit\n"
	"  -x\t\tExperimental keyboard settings; needs running X\n\n"
	"CLHexagon requires ncurses and glibc library to be installed\n\n"
	"=========================== Controls ============================\n"
	"Move\t\tarrow keys / A,D\n"
	"Quit\t\tQ / Esc\n"
	"Restart\t\tany other key\n"
	"=================================================================\n"
	"\nCopyright (C) 2013 Martin Scheubrein\n"
	"License GNU GPLv3 <http://gnu.org/licenses/gpl.html>\n"
	"For more info visit <http://matemag.xf.cz/clhexagon> (cz)\n\n",
	DELAY);
}

static void show_score(void)
{
	FILE *scorestream;
	int bestscore;

	scorestream = fopen(scorefile, "r");
	if (scorestream == NULL) {
		eputs("Can't open scores file\n");
		exit(1);
	}
	if (fscanf(scorestream, "%d", &bestscore) != 1) {
		eputs("Can't read scores file\n");
		fclose(scorestream);
		exit(1);
	}
	fclose(scorestream);

	printf("Best time is %d:%02d\n", bestscore/60, bestscore%60);
	exit(0);
}

/* Set keyboard (EXPERIMENTAL!) */
static void gotochar(FILE *file, char character)
{
	while (fgetc(file) != character)
		;
}

/* Set keyboard (EXPERIMENTAL!) */
static int setxmode(void)
{
	FILE *xsetq;
	char c;

	/* This shit echoes some parts of output of 'xset q' to TEMP_XSETQ */
	if (system("xset q | grep -m 2 repeat > " TEMP_XSETQ) == -1) {
		eputs("Unable to call system(), exiting -x mode\n");
		return 1;
	}
	xsetq = fopen(TEMP_XSETQ, "r");
	if (xsetq == NULL) {
		eputs("Unable to open tmp file, exiting -x mode\n");
		unlink(TEMP_XSETQ);
		return 1;
	}

	gotochar(xsetq, ':');
	gotochar(xsetq, 'o');
	c = fgetc(xsetq);

	/* Is auto repeating on or off? */
	switch (c) {
	case 'f':
		arpt = 0;
		fclose(xsetq);
		unlink(TEMP_XSETQ);
		return 0;
	case 'n':
		arpt = 1;
		break;
	default:
		eputs("Unexpected 'xset q' format, exiting X mode\n");
		fclose(xsetq);
		unlink(TEMP_XSETQ);
		return 1;
	}

	int xrate, xdelay;

	gotochar(xsetq, '\n');
	gotochar(xsetq, ':');

	/* Values of keyboard rate & delay */
	if (fscanf(xsetq, " %d repeat rate: %d", &xdelay, &xrate) != 2) {
		eputs("Unexpected 'xset q' format, exiting X mode\n");
		fclose(xsetq);
		unlink(TEMP_XSETQ);
		return 1;
	}
	fclose(xsetq);

	/* Reopen to write down plain values */
	xsetq = fopen(TEMP_XSETQ, "w");
	if (xsetq == NULL) {
		eputs("Unable to open tmp file, exiting -x mode\n");
		unlink(TEMP_XSETQ);
		fclose(xsetq);
		return 1;
	}
	fprintf(xsetq, "%d %d", xdelay, xrate);
	fclose(xsetq);

	return 0;
}

static void parse_options(int argc, char *argv[], int *debug, int *delay)
{
	int flag;

	while ((flag = getopt(argc, argv, "ghd:vxs")) != -1) {
		switch (flag) {
		case '?':
		case 'h':
			print_help();
			exit(0);
		case 'v':
			printf("clhexagon %d.%d\n", MAJOR_VERSION,
								MINOR_VERSION);
			exit(0);
		case 'g':
			*debug = DBGBAR_WIDTH;
			break;
		case 'd':
			*delay = atoi(optarg);
			break;
		case 'x':
			if (setxmode())
				arpt = -1;
			break;
		case 's':
			show_score();
			exit(0);
		}
	}
}

static void init_ncurses(void)
{
	initscr();		/* initialize */
	noecho();		/* no echoing getch() */
	keypad(stdscr, TRUE);	/* enable arrows */
	curs_set(0);		/* invisible cursor */
	timeout(TIMEOUT);	/* don't wait too long for getch() */
}

void print_dbg_info(int player_pos, int delay, int time)
{
	mvprintw(st_row+0, 0, "DEBUG INFO ");
	mvprintw(st_row+2, 0, "W: %3dx%3d ", ws_row, ws_col);
	mvprintw(st_row+3, 0, "Pl pos: %2d ", player_pos);
	mvprintw(st_row+4, 0, "Time: %4d ", time);
	mvprintw(st_row+6, 0, "Dely: %4d ", delay);

	if (arpt != -1)
		mvprintw(st_row+9, 0, "Autorpt: %d ", arpt);

	int layer, segment;

	move(st_row+11, 0);
	printw("N: ");
	for (segment = 0; segment < 6; segment++)
		printw("%c", hexagon[FUTURE][segment] ? 'X' : '.');

	for (layer = 0; layer < FUTURE; layer++) {
		move(st_row+11+FUTURE-layer, 0);	/* It draws from end */
		printw("%d: ", layer);
		for (segment = 0; segment < 6; segment++)
			printw("%c", hexagon[layer][segment] ? 'X' : '.');
	}
	mvprintw(st_row+11+FUTURE, player_pos/2+3, "o");
}

static int message(const char *msg)
{
	int i;

	move(st_row+23, st_col);
	for (i = 0; i < 46; i++)
		addch(' ');

	if (strlen(msg) > 46)
		return 1;

	mvprintw(st_row+23, st_col+23-strlen(msg)/2, msg);
	return 0;
}

int write_score(const int score)
{
	FILE *scorestream;
	int bestscore = 0;

	/* Open stream and read it */
	scorestream = fopen(scorefile, "r");
	if (scorestream == NULL) {
		scorestream = fopen(scorefile, "w");
		if (scorestream == NULL) {
			message("Can't create the scores file");
			return 1;
		}
		fprintf(scorestream, "%d", 0);
	} else {
		if (fscanf(scorestream, "%d", &bestscore) != 1)
			message("Can't read the scores file");
	}
	fclose(scorestream);

	/* Write and inform the user */

	char msg[46];

	if (score > bestscore) {
		sprintf(msg, "New best time: %d:%02d!", score/60, score%60);
		message(msg);

		scorestream = fopen(scorefile, "w");
		if (scorestream == NULL) {
			message("Can't write to the scores file");
			return 1;
		}
		fprintf(scorestream, "%d", score);
		fclose(scorestream);
	} else {
		sprintf(msg, "Best time is %d:%02d",
						bestscore/60, bestscore%60);
		message(msg);
	}
	return 0;
}

void quitxmode(void)
{
	char command[34];
	FILE *xsetq;
	int xdelay, xrate;

	xsetq = fopen(TEMP_XSETQ, "r");
	if (xsetq == NULL || fscanf(xsetq, "%d %d", &xdelay, &xrate) != 2) {
		eputs("Unable to open tmp file, setting default values of "
			"kbdrate (300, 30)\nThey can be repaired by typing "
			"'xset r rate <delay> <rate>'\n");
		xdelay = 300;
		xrate = 30;
	}
	fclose(xsetq);

	sprintf(command, "xset r rate %d %d", xdelay, xrate);
	if (system(command) == -1)
		eputs("Can't call system(), your kbdrate stays screwed\n");

	unlink(TEMP_XSETQ);
}

int main(int argc, char *argv[])
{
	/* Scorefile */
	scorefile = (char *)malloc((strlen(
					getenv("HOME")+strlen(SCOREFILE_PATH)
					))*sizeof(char));
	if (scorefile == NULL) {
		fprintf(stderr, "There isn't enough memory for "
							"scorefile string\n");
		exit(1);
	}
	scorefile = getenv("HOME");
	strcat(scorefile, SCOREFILE_PATH);

	int debug = 0;		/* Debugging mod (0 = false) */
	int delay = DELAY;	/* Delay between barriers' moves */

	parse_options(argc, argv, &debug, &delay);

	init_ncurses();
	getmaxyx(stdscr, ws_row, ws_col);
	st_row = ws_row/2-12;
	st_col = (ws_col+debug)/2-23;

	if (ws_row < 24 || ws_col < 46+debug) {
		endwin();
		fprintf(stderr, "This screen is %dx%d. Minimum is 24x%d.\n",
						ws_row, ws_col, 46+debug);
		return 1;
	}

	/* Initialize x mode if needed */
	if (arpt != -1)
		if (system("xset r rate 120 20") == -1) {
			eputs("Unable to call system(), exiting -x mode\n");
			arpt = -1;
		}

	/* Start the game */
	while (play_game(debug, delay))
		;

	if (arpt != -1)
		quitxmode();
	endwin();
	return 0;
}
